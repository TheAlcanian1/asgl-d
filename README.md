# What is `asgl-d`?

`asgl-d` or Arctic's Simple Game Library Dialog is a script written in bash made for managing games.  Arctic felt that you shouldn't have to use GUIs whenever you wanted to simply open a game so he made `asgl`.  `asgl` allows you to simply add, list, and play your games from it.

`asgl-d` is a fork of the original `asgl` that adds user friendliness to `asgl` by using Dialog instead of normal menus.

# Installation
Installation of `asgl-d` is very simple, all you need to do is move or copy it to your `$PATH` for example `/usr/local/bin`.

# How to use `asgl-d`
#### The Main Menu
This menu holds all `asgl-d` options, like Add and Remove. Use the Arrow Keys (or the mouse, but it's kind of spotty) to navigate.

![ASGLDMenu](https://gitlab.com/TheAlcanian1/asgl-d/raw/master/Screenshots/ASGLDMenu.png)

#### Adding games
To add a game, enter the Add menu and choose a name. Next, add the path to the game's executable (or a shell script to execute it).

![ASGLDAdd](https://gitlab.com/TheAlcanian1/asgl-d/raw/master/Screenshots/ASGLDAdd.png)

#### Removing games
To remove a game, enter the Remove menu and enter the name of a game you want removed, then press the Yes button.

![ASGLDRemove](https://gitlab.com/TheAlcanian1/asgl-d/raw/master/Screenshots/ASGLDRemove.png)

#### Listing games
To see all of your games, enter the List menu.

![ASGLDList](https://gitlab.com/TheAlcanian1/asgl-d/raw/master/Screenshots/ASGLDList.png)

#### Playing games
To play a game, enter the Play menu and select the game you want to play. The game's Shell Script file will be executed.

![ASGLDPlay](https://gitlab.com/TheAlcanian1/asgl-d/raw/master/Screenshots/ASGLDPlay.png)

# Future Plans
-Add categorys

-Improve the list command

-Port to another language

-Add the ability to import `.desktop` files
